#.
TEMPLATE        = app

CONFIG          += console 
CONFIG          -= debug_and_release debug
QT              -= gui

OBJECTS_DIR     = ../../build/bin/anymaster/obj
MOC_DIR         = ../../build/bin/anymaster/moc

DEPENDDIR       = .
INCLUDEDIR      = .

DEPENDPATH      += .
INCLUDEPATH     += ../../qserialdevice

QMAKE_LIBDIR    += ../../build/lib/qintegratedserialdevice/release

SOURCES         += anymaster.cpp \
                    main.cpp 
                    
HEADERS         += anymaster.h

LIBS            += -lqserialdevice

DESTDIR         = ../../build/bin/anymaster/release
TARGET          = anymaster

VERSION         = 0.2.0

win32:RC_FILE   += version.rc
