#.
TEMPLATE        = app

CONFIG          += console 
CONFIG          -= debug_and_release debug
QT              -= gui

OBJECTS_DIR     = ../../build/bin/anyslave/obj
MOC_DIR         = ../../build/bin/anyslave/moc

DEPENDDIR       = .
INCLUDEDIR      = .

DEPENDPATH      += .
INCLUDEPATH     += ../../qserialdevice

QMAKE_LIBDIR    += ../../build/lib/qintegratedserialdevice/release

SOURCES         += anyslave.cpp \
                    main.cpp 
                    
HEADERS         += anyslave.h

LIBS            += -lqserialdevice

DESTDIR         = ../../build/bin/anyslave/release
TARGET          = anyslave

VERSION         = 0.2.0

win32:RC_FILE   += version.rc
