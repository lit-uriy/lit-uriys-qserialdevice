#
TEMPLATE = subdirs
CONFIG   += ordered
SUBDIRS  = examples/reader \
           examples/sreader \
           examples/writer \
           examples/serialdeviceinfo \
           examples/serialdevicewatcher \
           examples/anymaster \
           examples/anyslave


