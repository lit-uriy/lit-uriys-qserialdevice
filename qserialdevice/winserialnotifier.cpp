/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/

#include <QtCore/private/qwineventnotifier_p.h>

#include "winserialnotifier.h"

#define WINSERIALNOTIFIER_DEBUG

#ifdef WINSERIALNOTIFIER_DEBUG
#include <QtCore/QDebug>
#endif


WinSerialNotifier::WinSerialNotifier(HANDLE hd, Type type, QObject *parent)
        : QObject(parent), shd(hd), wsntype(type), wsnenabled(true)
{
    if ( INVALID_HANDLE_VALUE == this->shd ) {
#if defined (WINSERIALNOTIFIER_DEBUG)
        qDebug() << "Windows: WinSerialNotifier::WinSerialNotifier(...) \n"
                " -> hd == INVALID_HANDLE_VALUE. Error! \n";
#endif
        return;
    }

    this->eventMask = 0;
    this->wen = 0;

    ::memset(&this->ovl, 0, sizeof(this->ovl));

    this->ovl.hEvent = ::CreateEvent(0, false, false, 0);

    if ( 0 == this->ovl.hEvent ) {
#if defined (WINSERIALNOTIFIER_DEBUG)
        qDebug() << "Windows: WinSerialNotifier::WinSerialNotifier(...) \n"
                " -> function: ::CreateEvent(0, false, false, 0) returned 0. Error! \n";
#endif
        return;
    }

    this->wen = new QWinEventNotifier(this->ovl.hEvent, this);
    connect(this->wen, SIGNAL(activated(HANDLE)), this, SLOT(updateNotifier(HANDLE)));

    this->setEnabled(this->wsnenabled);
}

/*!
    Destroys the WinSerialNotifier.
*/
WinSerialNotifier::~WinSerialNotifier()
{
    this->setEnabled(false);

    if (this->ovl.hEvent) {
        if ( 0 == ::CloseHandle(this->ovl.hEvent) ) {
#if defined (WINSERIALNOTIFIER_DEBUG)
            qDebug() << "Windows: WinSerialNotifier::~WinSerialNotifier() \n"
                    " -> function: ::CloseHandle(this->ovl.hEvent) returned: 0. Error! \n";
#endif
        }
        this->ovl.hEvent = 0;
    }
}

void WinSerialNotifier::updateNotifier(HANDLE h)
{
    if (!this->isValid())
        return;

    if (h == this->ovl.hEvent) {
        switch (this->wsntype) {
        case Read:
            if ( EV_RXCHAR == (this->eventMask & EV_RXCHAR) ) {
                DWORD err = 0;
                COMSTAT cs;
                ::memset(&cs, 0, sizeof(cs));

                if ( 0 == ::ClearCommError(this->shd, &err, &cs) ) {
#if defined (WINSERIALNOTIFIER_DEBUG)
                    qDebug() << "Windows: WinSerialNotifier::updateNotifier(HANDLE h) \n"
                            " -> function: ::ClearCommError(this->shd, &err, &cs) returned: 0. Error! \n";
#endif
                    break;
                }

                if (err) {
#if defined (WINSERIALNOTIFIER_DEBUG)
                    qDebug() << "Windows: WinSerialNotifier::updateNotifier(HANDLE h) \n"
                            " -> in function: ::ClearCommError(this->shd, &err, &cs), \n"
                            " output parameter - err: " << err << " \n";
#endif
                    break;
                }

                if (cs.cbInQue > 0)
                    emit this->activated(this->shd);
            }
            break;
        case Write:
            if ( EV_TXEMPTY == (this->eventMask & EV_TXEMPTY) )
                emit this->activated(this->shd);
            break;
        default:
            ;
        }
        this->setEnabled(true);
    }
}

void WinSerialNotifier::setEnabled(bool enable)
{
    if (!this->isValid())
        return;

    if (enable) {
        if ( 0 == ::GetCommMask(this->shd, &this->eventMask) ) {
#if defined (WINSERIALNOTIFIER_DEBUG)
            qDebug() << "Windows: WinSerialNotifier::setEnabled(bool enable) \n"
                    " -> function: ::GetCommMask(this->shd, &this->eventMask), \n "
                    " this->eventMask: " <<  this->eventMask << " returned: 0. Error! \n";
#endif
        }

        switch (this->wsntype) {
        case Read:
            if ( 0 == (this->eventMask & EV_RXCHAR) )
                this->eventMask |= EV_RXCHAR;
            break;
        case Write:
            if ( 0 == (this->eventMask & EV_TXEMPTY) )
                this->eventMask |= EV_TXEMPTY;
            break;
        default:
            ;
        }

        if ( 0 == ::SetCommMask(this->shd, this->eventMask) ) {
#if defined (WINSERIALNOTIFIER_DEBUG)
            qDebug() << "Windows: WinSerialNotifier::setEnabled(bool enable) \n"
                    " -> function: ::SetCommMask(this->shd, this->eventMask), \n "
                    " this->eventMask: " <<  this->eventMask << " returned: 0. Error! \n";
#endif
            return;
        }

        ::WaitCommEvent(this->shd, &this->eventMask, &this->ovl);
    }

    this->wen->setEnabled(enable);
    this->wsnenabled = enable;
}
