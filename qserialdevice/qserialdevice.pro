#
#PROJECT        = Serial Device Liblary
TEMPLATE        = lib

CONFIG          += staticlib
CONFIG          -= debug_and_release debug
QT              -= gui

OBJECTS_DIR     = ../build/lib/qserialdevice/obj
MOC_DIR         = ../build/lib/qserialdevice/moc

include(qserialdevice.pri)

#TODO: here in future replace
#contains( CONFIG, staticlib ) {
#    win32:DEFINES += QSERIALDEVICE_STATIC
#}

#TODO: here in future replace
contains( CONFIG, dll ) {
    win32:DEFINES += QSERIALDEVICE_SHARED
}

DESTDIR         = ../build/lib/qserialdevice/release
TARGET          = qserialdevice


VERSION         = 0.2.0

win32:RC_FILE   += version.rc
