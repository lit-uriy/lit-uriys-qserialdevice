/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/

#include <QtCore/QAbstractEventDispatcher>

#include <qt_windows.h>

#include "nativeserialengine.h"


//#define NATIVESERIALENGINE_WIN_DEBUG

#ifdef NATIVESERIALENGINE_WIN_DEBUG
#include <QtCore/QDebug>
#endif


NativeSerialEnginePrivate::NativeSerialEnginePrivate()
        : hd(INVALID_HANDLE_VALUE), readNotifier(0)
{
    ::memset((void *)(&this->cc), 0, sizeof(this->cc));
    ::memset((void *)(&this->oldcc), 0, sizeof(this->oldcc));
    ::memset((void *)(&this->ct), 0, sizeof(this->ct));
    ::memset((void *)(&this->oldct), 0, sizeof(this->oldct));

    ::memset((void *)(&this->oRx), 0 ,sizeof(this->oRx));
    ::memset((void *)(&this->oTx), 0 ,sizeof(this->oTx));
    ::memset((void *)(&this->oSelect), 0 ,sizeof(this->oSelect));

    this->hEvRx = this->hEvTx = this->hEvSelect = 0;
    this->m_deviceName = this->m_defaultDeviceName;
    //
    this->m_openMode = AbstractSerial::NotOpen;
    this->m_status = AbstractSerial::ENone;
    this->m_oldSettingsIsSaved = false;
}

bool NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
{
    if (this->isValid()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> the device is already open or other error. Error!";
#endif
        return false;
    }

    DWORD access = 0;
    DWORD sharing = 0;
    bool evRx = false;
    bool evTx = false;

    switch (mode) {
    case AbstractSerial::ReadOnly:
        access = GENERIC_READ; //sharing = FILE_SHARE_READ;
        evRx = true;
        break;
    case AbstractSerial::WriteOnly:
        access = GENERIC_WRITE; //sharing = FILE_SHARE_WRITE;
        evTx = true;
        break;
    case AbstractSerial::ReadWrite:
        access = GENERIC_READ | GENERIC_WRITE; //sharing = FILE_SHARE_READ | FILE_SHARE_WRITE;
        evRx = evTx = true;
        break;
    default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> mode: " << mode << " undefined. Error! \n";
#endif
        return false;
    }

    //try opened serial device
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
    qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
            " -> trying to open device: " << this->m_deviceName << "\n";
#endif

    //Create the file handle.
    QString path = "\\\\.\\" + this->m_deviceName;
    QByteArray nativeFilePath = QByteArray((const char *)path.utf16(), path.size() * 2 + 1);

    this->hd = ::CreateFile((const wchar_t*)nativeFilePath.constData(),
                            access,
                            sharing,
                            0,
                            OPEN_EXISTING,
                            FILE_FLAG_OVERLAPPED,
                            0);

    if (!this->isValid()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: ::CreateFile(...) returned: " << this->hd << ", \n"
                " last error is: " << ::GetLastError() << ". Error! \n";
#endif
        return false;
    }

/*
    if (!this->nativeReset()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: nativeReset() returned: false. Error! \n";
#endif
        return false;
    }
*/

    if (!this->saveOldSettings()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: saveOldSettings() returned: false. Error! \n";
#endif
        return false;
    }

    //Prepare other options
    this->prepareOtherOptions();

    if (!this->setDefaultAsyncCharTimeout()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: setDefaultAsyncCharTimeout() returned: false. Error! \n";
#endif
        return false;
    }

    if ( 0 == ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) returned: 0. Error! \n";
#endif
        return false;
    }

    if (!this->detectDefaultCurrentSettings()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: detectDefaultCurrentSettings() returned: false. Error! \n";
#endif
        return false;
    }

    if (!this->createEvents(evRx, evTx)) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: createEvents(evRx, evTx) returned: false. Error! \n";
#endif
        return false;
    }

#if defined (NATIVESERIALENGINE_WIN_DEBUG)
    qDebug() << "Windows: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
            " -> opened device: " << this->m_deviceName << " in mode: " << mode << " succesfully. Ok! \n";
#endif

    this->m_openMode = mode;
    return true;
}

bool NativeSerialEnginePrivate::nativeClose()
{
    if (!this->isValid()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeClose() \n"
               " -> descriptor is invalid. Error! \n");
#endif
        return false;
    }

    bool closeResult = true;

    //try restore old parameters device
    if (!this->restoreOldSettings()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeClose() \n"
               " -> function: restoreOldSettings() returned: false. Error! \n");
#endif
        closeResult = false;
    }

    ::CancelIo(this->hd);
    //try closed device
    if (0 == ::CloseHandle(this->hd) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeClose() \n"
               " -> function: ::CloseHandle(this->hd) returned: 0. Error! \n");
#endif
        closeResult = false;
    }

    if (!this->closeEvents())
        closeResult = false;

    if (closeResult) {
        this->m_openMode = AbstractSerial::NotOpen;
        this->hd = INVALID_HANDLE_VALUE;
    }
    
    return closeResult;
}

bool NativeSerialEnginePrivate::nativeSetBaudRate(AbstractSerial::BaudRate baudRate)
{
    switch (baudRate) {
    case AbstractSerial::BaudRate50:
    case AbstractSerial::BaudRate75:
    case AbstractSerial::BaudRate134:
    case AbstractSerial::BaudRate150:
    case AbstractSerial::BaudRate200:
    case AbstractSerial::BaudRate1800:
    case AbstractSerial::BaudRate76800:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> windows does not support" << baudRate << "baud operation. Error! \n";
#endif
        return false;
    case AbstractSerial::BaudRate110: this->cc.dcb.BaudRate = CBR_110; break;
    case AbstractSerial::BaudRate300: this->cc.dcb.BaudRate = CBR_300; break;
    case AbstractSerial::BaudRate600: this->cc.dcb.BaudRate = CBR_600; break;
    case AbstractSerial::BaudRate1200: this->cc.dcb.BaudRate = CBR_1200; break;
    case AbstractSerial::BaudRate2400: this->cc.dcb.BaudRate = CBR_2400; break;
    case AbstractSerial::BaudRate4800: this->cc.dcb.BaudRate = CBR_4800; break;
    case AbstractSerial::BaudRate9600: this->cc.dcb.BaudRate = CBR_9600; break;
    case AbstractSerial::BaudRate14400: this->cc.dcb.BaudRate = CBR_14400; break;
    case AbstractSerial::BaudRate19200: this->cc.dcb.BaudRate = CBR_19200; break;
    case AbstractSerial::BaudRate38400: this->cc.dcb.BaudRate = CBR_38400; break;
    case AbstractSerial::BaudRate56000: this->cc.dcb.BaudRate = CBR_56000; break;
    case AbstractSerial::BaudRate57600: this->cc.dcb.BaudRate = CBR_57600; break;
    case AbstractSerial::BaudRate115200: this->cc.dcb.BaudRate = CBR_115200; break;
    case AbstractSerial::BaudRate128000: this->cc.dcb.BaudRate = CBR_128000; break;
    case AbstractSerial::BaudRate256000: this->cc.dcb.BaudRate = CBR_256000; break;
    default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> baudRate: " << baudRate << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::BaudRate. Error! \n";
#endif
        return false;
    }//switch

    if (0 == ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> function: ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) returned: 0. Error! \n";
#endif
        return false;
    }
    this->m_ibaudRate = this->m_obaudRate = baudRate;//!!!
    return true;
}

bool NativeSerialEnginePrivate::nativeSetInputBaudRate(AbstractSerial::BaudRate baudRate)
{
    //This option is not supported in Windows (?)
    return false;
}

bool NativeSerialEnginePrivate::nativeSetOutputBaudRate(AbstractSerial::BaudRate baudRate)
{
    //This option is not supported in Windows (?)
    return false;
}

bool NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits)
{
    if ( (AbstractSerial::DataBits5 == dataBits)
        && (AbstractSerial::StopBits2 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 5 data bits cannot be used with 2 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits6 == dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 6 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits7 == dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 7 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits8 == dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 8 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    switch (dataBits) {
    case AbstractSerial::DataBits5: this->cc.dcb.ByteSize = 5; break;
    case AbstractSerial::DataBits6: this->cc.dcb.ByteSize = 6; break;
    case AbstractSerial::DataBits7: this->cc.dcb.ByteSize = 7; break;
    case AbstractSerial::DataBits8: this->cc.dcb.ByteSize = 8; break;
    default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
                " -> dataBits: " << dataBits << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::DataBits. Error! \n";
#endif
        return false;
    }//switch dataBits

    if ( 0 == ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
                " -> function: ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) returned: 0. Error! \n";
#endif
        return false;
    }
    this->m_dataBits = dataBits;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity)
{
    switch (parity) {
    case AbstractSerial::ParityNone: this->cc.dcb.Parity = NOPARITY; this->cc.dcb.fParity = false; break;
    case AbstractSerial::ParitySpace: this->cc.dcb.Parity = SPACEPARITY; this->cc.dcb.fParity = true; break;
    case AbstractSerial::ParityMark: this->cc.dcb.Parity = MARKPARITY; this->cc.dcb.fParity = true; break;
    case AbstractSerial::ParityEven: this->cc.dcb.Parity = EVENPARITY; this->cc.dcb.fParity = true; break;
    case AbstractSerial::ParityOdd: this->cc.dcb.Parity = ODDPARITY; this->cc.dcb.fParity = true; break;
    default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                " -> parity: " << parity << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::Parity. Error! \n";
#endif
        return false;
    }//switch parity

    if ( 0 == ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                " -> function: ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) returned: 0. Error! \n";
#endif
        return false;
    }
    this->m_parity = parity;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits)
{
    if ( (AbstractSerial::DataBits5 == this->m_dataBits)
        && (AbstractSerial::StopBits2 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
               " -> 5 data bits cannot be used with 2 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits6 == this->m_dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
               " -> 6 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits7 == this->m_dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
               " -> 7 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits8 == this->m_dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
               " -> 8 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    switch (stopBits) {
    case AbstractSerial::StopBits1: this->cc.dcb.StopBits = ONESTOPBIT; break;
    case AbstractSerial::StopBits1_5: this->cc.dcb.StopBits = ONE5STOPBITS; break;
    case AbstractSerial::StopBits2: this->cc.dcb.StopBits = TWOSTOPBITS; break;
    default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
                " -> stopBits: " << stopBits << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::StopBits. Error! \n";
#endif
        return false;
    }//switch stopBits

    if ( 0 == ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
                " -> function: ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) returned: 0. Error! \n";
#endif
        return false;
    }
    this->m_stopBits = stopBits;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetFlowControl(AbstractSerial::Flow flow)
{
    switch (flow) {
    case AbstractSerial::FlowControlOff:
        this->cc.dcb.fOutxCtsFlow = false; this->cc.dcb.fRtsControl = RTS_CONTROL_DISABLE;
        this->cc.dcb.fInX = this->cc.dcb.fOutX = false; break;
    case AbstractSerial::FlowControlXonXoff:
        this->cc.dcb.fOutxCtsFlow = false; this->cc.dcb.fRtsControl = RTS_CONTROL_DISABLE;
        this->cc.dcb.fInX = this->cc.dcb.fOutX = true; break;
    case AbstractSerial::FlowControlHardware:
        this->cc.dcb.fOutxCtsFlow = true; this->cc.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
        this->cc.dcb.fInX = this->cc.dcb.fOutX = false; break;
    default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetFlowControl(AbstractSerial::Flow flow) \n"
                " -> flow: " << flow << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::Flow. Error! \n";
#endif
        return false;
    }//switch flow

    if ( 0 == ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSetFlowControl(AbstractSerial::Flow flow) \n"
                " -> function: ::SetCommConfig(this->hd, &this->cc, sizeof(COMMCONFIG)) returned: 0. Error! \n";
#endif
        return false;
    }
    this->m_flow = flow;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetCharIntervalTimeout(int msecs)
{
    this->m_charIntervalTimeout = msecs;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetDtr(bool set)
{
    if ( -1 == ::EscapeCommFunction(this->hd, (set) ? SETDTR : CLRDTR) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetDtr(bool set) \n"
               " -> function: ::EscapeCommFunction(...) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

bool NativeSerialEnginePrivate::nativeSetRts(bool set)
{
    if ( -1 == ::EscapeCommFunction(this->hd, (set) ? SETRTS : CLRRTS) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetRts(bool set) \n"
               " -> function: ::EscapeCommFunction(...) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

ulong NativeSerialEnginePrivate::nativeLineStatus()
{
    DWORD temp = 0;
    ulong status = 0;

    if ( 0 == ::GetCommModemStatus(this->hd, &temp) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeLineStatus() \n"
               " -> function: ::GetCommModemStatus(...) returned: 0. Error! \n");
#endif
        return status;
    }

    if (temp & MS_CTS_ON) status |= AbstractSerial::lsCTS;
    if (temp & MS_DSR_ON) status |= AbstractSerial::lsDSR;
    if (temp & MS_RING_ON) status |= AbstractSerial::lsRI;
    if (temp & MS_RLSD_ON) status |= AbstractSerial::lsDCD;

    return status;
}

bool NativeSerialEnginePrivate::nativeSendBreak(int duration)
{
    //TODO: help implementation?!
    if (this->nativeSetBreak(true)) {
        ::Sleep(DWORD(duration));
        if (this->nativeSetBreak(false))
            return true;
    }
    return false;
}

bool NativeSerialEnginePrivate::nativeSetBreak(bool set)
{
    if (set) {
        if ( 0 == ::SetCommBreak(this->hd) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug("Windows: NativeSerialEnginePrivate::nativeSetBreak(bool set) \n"
                   " -> function: ::SetCommBreak(this->hd) returned: 0. Error! \n");
#endif
            return false;
        }
        return true;
    }

    if ( 0 == ::ClearCommBreak(this->hd) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeSetBreak(bool set) \n"
               " -> function: ::ClearCommBreak(this->hd) returned: 0. Error! \n");
#endif
        return false;
    }
    return true;
}

bool NativeSerialEnginePrivate::nativeFlush()
{
    bool ret = ::FlushFileBuffers(this->hd);
    if (!ret) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeFlush() \n"
               " -> function:: ::FlushFileBuffers(this->hd) returned: false. Error! \n");
#endif
        ;
    }
    return ret;
}

bool NativeSerialEnginePrivate::nativeReset()
{
    bool ret = true;
    DWORD flags = (PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    if ( 0 == ::PurgeComm(this->hd, flags) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Windows: NativeSerialEnginePrivate::nativeReset() \n"
               " -> function: ::PurgeComm(...) returned: 0. Error! \n");
#endif
        ret = false;
    }

    if (!this->nativeSetBreak(false))
        ret = false;
    return ret;
}

qint64 NativeSerialEnginePrivate::nativeBytesAvailable()
{
    DWORD err = 0;
    COMSTAT cs = {0};
    qint64 bav = -1;

    for (;;) {
        if ( 0 == ::ClearCommError(this->hd, &err, &cs) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug("Windows: NativeSerialEnginePrivate::nativeBytesAvailable(bool wait) \n"
                   " -> function: ::ClearCommError(this->hd, &err, &cs) returned: 0. Error! \n");
#endif
            break;
        }
        if (err) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug() << "Windows: NativeSerialEnginePrivate::nativeBytesAvailable(bool wait) \n"
                    " -> in function: ::ClearCommError(this->hd, &err, &cs), \n"
                    " output parameter: err: " << err << ". Error! \n";
#endif
            break;
        }
        bav = qint64(cs.cbInQue);
        break;
    }
    return bav;
}

qint64 NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len)
{
    if (0 == this->hEvTx ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) \n"
                " -> write event is invalid: this->hEvTx == 0. Error! \n";
#endif
        return qint64(-1);
    }

    ::memset((void *)(&this->oTx), 0 ,sizeof(this->oTx));
    this->oTx.hEvent = this->hEvTx;

    DWORD writeBytes = 0;
    bool sucessResult = false;

    if (::WriteFile(this->hd, (PCVOID)data, (DWORD)len, &writeBytes, &this->oTx))
        sucessResult = true;
    else {
        DWORD rc = ::GetLastError();
        if ( ERROR_IO_PENDING == rc ) {
            //not to loop the function, instead of setting INFINITE put 5000 milliseconds wait!
            rc = ::WaitForSingleObject(this->oTx.hEvent, 5000);
            switch (rc) {
            case WAIT_OBJECT_0:
                if (::GetOverlappedResult(this->hd, &this->oTx, &writeBytes, false))
                    sucessResult = true;
                else {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                    qDebug("Windows: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) \n"
                           " -> function: ::GetOverlappedResult(this->hd, &this->oTx, &writeBytes, false) returned: false. Error! \n");
#endif
                    ;
                }
                break;//WAIT_OBJECT_0
            default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                qDebug() << "Windows: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) \n"
                        " -> function: ::::WaitForSingleObject(this->oTx.hEvent, 5000) returned: " << rc << ". Error! \n";
#endif
                ;
            }//switch (rc)
        }
        else {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug() << "Windows: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) \n"
                    " -> function: ::GetLastError() returned: " << rc << ". Error! \n";
#endif
            ;
        }
    }

    return (sucessResult) ? quint64(writeBytes) : qint64(-1);
}

qint64 NativeSerialEnginePrivate::nativeRead(char *data, qint64 len)
{
    if ( 0 == this->hEvRx ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) \n"
                " -> write event is invalid: this->hEvRx == 0. Error! \n";
#endif
        return qint64(-1);
    }

    ::memset((void *)(&this->oRx), 0 ,sizeof(this->oRx));
    this->oRx.hEvent = this->hEvRx;

    DWORD readBytes = 0;
    bool sucessResult = false;

    if (::ReadFile(this->hd, (PVOID)data, (DWORD)len, &readBytes, &this->oRx))
        sucessResult = true;
    else {
        DWORD rc = ::GetLastError();
        if ( ERROR_IO_PENDING == rc ) {
            //not to loop the function, instead of setting INFINITE put 5000 milliseconds wait!
            rc = ::WaitForSingleObject(this->oRx.hEvent, 5000);
            switch (rc) {
            case WAIT_OBJECT_0:
                if (::GetOverlappedResult(this->hd, &this->oRx, &readBytes, false))
                    sucessResult = true;
                else {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                    qDebug("Windows: NativeSerialEnginePrivate::nativeRead(char *data, qint64 len) \n"
                           " -> function: ::GetOverlappedResult(this->hd, &this->oRx, &readBytes, false) returned: false. Error! \n");
#endif
                    ;
                }
                break;
            default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                qDebug() << "Windows: NativeSerialEnginePrivate::nativeRead(char *data, qint64 len) \n"
                        " -> function: ::::WaitForSingleObject(this->oRx.hEvent, 5000) returned: " << rc << ". Error! \n";
#endif
                ;
            }
        } // ERROR_IO_PENDING
        else {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug() << "Windows: NativeSerialEnginePrivate::nativeRead(char *data, qint64 len) \n"
                    " -> function: ::GetLastError() returned: " << rc << ". Error! \n";
#endif
            ;
        }
    }

    return (sucessResult) ? qint64(readBytes) : qint64(-1);
}

int NativeSerialEnginePrivate::nativeSelect(int timeout,
                                            bool checkRead, bool checkWrite,
                                            bool *selectForRead, bool *selectForWrite)
{
    /*
    TODO: This "crutch". The idea is this:
    1)
    Example:
    1. Suppose we use WaitForReadyRead to monitor the arrival of bytes.
    2. Function "qint64 read(10)" we have configured to read 10-byte.
    3. At the end of nativeSelect() we switched off the monitor "::SetCommMask(this->hd, 0)"
    4. The port accounted for 11 bytes.

    Then:
    1. The first call WaitForReadyRead() returns true and read function reads 10 bytes
    (but was still 1 byte in buffer). While all true!
    2. The second challenge WaitForReadyRead returns false!!! Therefore,
    we can not know that the buffer is still 1 byte.
    This is wrong, because a must read for another 1 bytes,
    ie ideally, should get 10+1 bytes! But this is not happening!

    To remedy this, I entered the code (crutch).

    PS: most interesting thing is that if the port is 12 bytes,
    then the function WaitForReadyRead works off properly (ie, read the 10 + 2 bytes).

    2)
    Example:
    1. Suppose we use WaitForReadyRead to monitor the arrival of bytes.
    2. Function "qint64 read(10)" we have configured to read 10-byte.
    3. At the end nativeSelect() we do NOT switch off your monitor "::SetCommMask (this->hd, evantMask)"

    4. Now, if the port comes 11 bytes - it is read correctly 10+1 bytes,
    but if the port comes 10 bytes, then:
        - First call WaitForReadyRead() correctly returns "true"
        and we will read 10 bytes (ie, in the receive buffer is no longer byte)
        - But the second challenge WaitForReadyRead() will return once more "true",
        although in the receive buffer 0 bytes, ie falsely triggered.
    */
    if (this->nativeBytesAvailable()) { //<< this "crutch"
        *selectForRead = true;
        return 1;
    }

    int ret = -1;

    if ( 0 == this->hEvSelect ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) \n"
                " -> write event is invalid: this->oSelect.hEvent == 0. Error! \n";
#endif
        return ret;
    }

    ::memset((void *)(&this->oSelect), 0, sizeof(this->oSelect));
    this->oSelect.hEvent = this->hEvSelect;

    bool selectResult = false;
    DWORD eventMask = (EV_RXCHAR | EV_TXEMPTY);

    //set mask
    if( 0 == ::SetCommMask(this->hd, eventMask) ) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::nativeSelect(...) \n"
                " -> function: ::SetCommMask(this->hd, eventMask) returned: 0. Error! \n";
#endif
        return ret;
    }

    if (::WaitCommEvent(this->hd, &eventMask, &this->oSelect))
        selectResult = true;
    else {
        DWORD rc = ::GetLastError();
        if ( ERROR_IO_PENDING == rc ) {
            rc = ::WaitForSingleObject(this->oSelect.hEvent, (timeout < 0) ? 0 : timeout);
            switch (rc) {
            case WAIT_OBJECT_0:
                if (::GetOverlappedResult(this->hd, &this->oSelect, &rc, false))
                    selectResult = true;
                else {
                    rc = ::GetLastError();
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                    qDebug() << "Windows: NativeSerialEnginePrivate::nativeSelect(...) \n"
                            " -> function: ::GetOverlappedResult(this->hd, &this->oSelect, &rc, false), \n"
                            " returned: false, last error: " << rc << ". Error! \n";
#endif
                    ;
                }
                break;
            case WAIT_TIMEOUT:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                qDebug() << "Windows: NativeSerialEnginePrivate::nativeSelect(...) \n"
                        " -> function: ::WaitForSingleObject(this->oSelect.hEvent, timeout < 0 ? 0 : timeout), \n"
                        " returned: WAIT_TIMEOUT: " << rc << ". Warning! \n";
#endif
                ret = 0;
                break;
            default:
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
                qDebug() << "Windows: NativeSerialEnginePrivate::nativeSelect(...) \n"
                        " -> function: ::WaitForSingleObject(this->oSelect.hEvent, timeout < 0 ? 0 : timeout), \n"
                        " returned: " << rc << ". Error! \n";
#endif
                ;
            }
        }
        else {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug() << "Windows: NativeSerialEnginePrivate::nativeSelect(...) \n"
                    " -> function: ::GetLastError() returned: " << rc << ". Error! \n";
#endif
            ;
        }
    }

    if (selectResult) {
        *selectForRead = (eventMask & EV_RXCHAR) ? true : false;
        *selectForWrite = (eventMask & EV_TXEMPTY) ? true : false;

        if ((*selectForRead) || (*selectForWrite))
            ret = 1;
        else {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
            qDebug() << "Windows: NativeSerialEnginePrivate::nativeSelect(...) \n"
                    " -> in function: ::WaitCommEvent(this->hd, &eventMask, &ovl), \n"
                    " undefined mask event eventMask: " << eventMask <<". Error! \n";
#endif
            ;
        }
    }

    ::SetCommMask(this->hd, 0); //switch off event monitor
    return ret;
}

//added 05.11.2009 (experimental)
qint64 NativeSerialEnginePrivate::nativeCurrentQueue(NativeSerialEngine::ioQueue Queue) const
{
    Q_UNUSED(Queue)
    /*
    COMMPROP commProp = {0};

    if ( !(::GetCommProperties(this->hd, &commProp)) ) {
    #if defined (NATIVESERIALENGINE_WIN_DEBUG)
    qDebug() << "Windows: NativeSerialEnginePrivate::nativeCurrentQueue(NativeSerialEngine::ioQueue Queue) \n"
    " -> function: ::GetCommProperties(this->hd, &commProp) returned: false. Error! \n";
    #endif
    return (qint64)-1;
    }

    switch (Queue) {
    case NativeSerialEngine::txQueue : return (qint64)commProp.dwCurrentTxQueue;
    case NativeSerialEngine::rxQueue : return (qint64)commProp.dwCurrentRxQueue;
    default: return (qint64)-1;
    }
    */
    return qint64(-1);
}

bool NativeSerialEnginePrivate::isValid() const
{
    return (this->hd != INVALID_HANDLE_VALUE);
}

/* Defines a parameter - the current baud rate of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultBaudRate()
{
    DWORD result = this->cc.dcb.BaudRate;
    switch (result) {
    case CBR_110: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate110; break;
    case CBR_300: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate300; break;
    case CBR_600: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate600; break;
    case CBR_1200: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate1200; break;
    case CBR_2400: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate2400; break;
    case CBR_4800: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate4800; break;
    case CBR_9600: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate9600; break;
    case CBR_14400: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate14400; break;
    case CBR_19200: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate19200; break;
    case CBR_38400: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate38400; break;
    case CBR_56000: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate56000; break;
    case CBR_57600: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate57600; break;
    case CBR_115200: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate115200; break;
    case CBR_128000: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate128000; break;
    case CBR_256000: this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRate256000; break;
    default:
        this->m_ibaudRate = this->m_obaudRate = AbstractSerial::BaudRateUndefined;
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::detectDefaultBaudRate() \n"
                " -> undefined baud rate, this->cc.dcb.BaudRate is: " << result << " \n";
#endif
        ;
    }
    return true;
}

/* Defines a parameter - the current data bits of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultDataBits()
{
    BYTE result = this->cc.dcb.ByteSize;
    switch (result) {
    case 5: this->m_dataBits = AbstractSerial::DataBits5; break;
    case 6: this->m_dataBits = AbstractSerial::DataBits6; break;
    case 7: this->m_dataBits = AbstractSerial::DataBits7; break;
    case 8: this->m_dataBits = AbstractSerial::DataBits8; break;
    default:
        this->m_dataBits = AbstractSerial::DataBitsUndefined;
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::detectDefaultDataBits() \n"
                " -> undefined data bits, this->cc.dcb.ByteSize is: " << result << " \n";
#endif
        ;
    }
    return true;
}

/* Defines a parameter - the current parity of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultParity()
{
    BYTE bparity = this->cc.dcb.Parity;
    DWORD fparity = this->cc.dcb.fParity;

    if ( (NOPARITY == bparity) && (!fparity) ) {
        this->m_parity = AbstractSerial::ParityNone;
        return true;
    }
    if ( (SPACEPARITY == bparity) && (fparity) ) {
        this->m_parity = AbstractSerial::ParitySpace;
        return true;
    }
    if ( (MARKPARITY == bparity) && (fparity) ) {
        this->m_parity = AbstractSerial::ParityMark;
        return true;
    }
    if ( (EVENPARITY == bparity) && (fparity) ) {
        this->m_parity = AbstractSerial::ParityEven;
        return true;
    }
    if ( (ODDPARITY == bparity) && (fparity) ) {
        this->m_parity = AbstractSerial::ParityOdd;
        return true;
    }
    this->m_parity = AbstractSerial::ParityUndefined;
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
    qDebug() << "Windows: NativeSerialEnginePrivate::detectDefaultParity() \n"
            " -> undefined parity, this->cc.dcb.Parity is: " << bparity << ", this->cc.dcb.fParity is: " << fparity << " \n";
#endif
    return true;
}

/* Defines a parameter - the current stop bits of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultStopBits()
{
    BYTE result = this->cc.dcb.StopBits;
    switch (result) {
    case ONESTOPBIT: this->m_stopBits = AbstractSerial::StopBits1; break;
    case ONE5STOPBITS: this->m_stopBits = AbstractSerial::StopBits1_5; break;
    case TWOSTOPBITS: this->m_stopBits = AbstractSerial::StopBits2; break;
    default:
        this->m_stopBits = AbstractSerial::StopBitsUndefined;
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug() << "Windows: NativeSerialEnginePrivate::detectDefaultStopBits() \n"
                " -> undefined stop bits, this->cc.dcb.StopBits is: " << result << " \n";
#endif
        ;
    }
    return true;
}

/* Defines a parameter - the current flow control of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultFlowControl()
{
    //TODO: here variables in future removed and replace
    DWORD f_OutxCtsFlow = this->cc.dcb.fOutxCtsFlow;
    DWORD f_RtsControl = this->cc.dcb.fRtsControl;
    DWORD f_InX = this->cc.dcb.fInX;
    DWORD f_OutX = this->cc.dcb.fOutX;

    if ( (!f_OutxCtsFlow) && (RTS_CONTROL_DISABLE == f_RtsControl)
        && (!f_InX) && (!f_OutX) ) {
        this->m_flow = AbstractSerial::FlowControlOff;
        return true;
    }
    if ( (!f_OutxCtsFlow) && (RTS_CONTROL_DISABLE == f_RtsControl)
        && (f_InX) && (f_OutX) ) {
        this->m_flow = AbstractSerial::FlowControlXonXoff;
        return true;
    }
    if ( (f_OutxCtsFlow) && (RTS_CONTROL_HANDSHAKE == f_RtsControl)
        && (!f_InX) && (!f_OutX) ) {
        this->m_flow = AbstractSerial::FlowControlHardware;
        return true;
    }
    this->m_flow = AbstractSerial::FlowControlUndefined;
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
    qDebug() << "Windows: NativeSerialEnginePrivate::detectDefaultFlowControl() \n"
            " -> undefined flow, this->cc.dcb.fOutxCtsFlow is: " << f_OutxCtsFlow
            << ", this->cc.dcb.fRtsControl is: " << f_RtsControl
            << ", this->cc.dcb.fInX is: " << f_InX
            << ", this->cc.dcb.fOutX is: " << f_OutX
            << " \n";
#endif
    return true;
}

/* Specifies the port settings that were with him by default when you open it.
   Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::detectDefaultCurrentSettings()
{
    return ( this->detectDefaultBaudRate() && this->detectDefaultDataBits() && this->detectDefaultParity()
             && this->detectDefaultStopBits() && this->detectDefaultFlowControl() );
}

/* Force sets the parameters of timeouts port for asynchronous mode.
   Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::setDefaultAsyncCharTimeout()
{
    /* TODO:
    here you need to specify the settings for the asynchronous mode! to check! */
    this->ct.ReadIntervalTimeout = MAXDWORD;
    this->ct.ReadTotalTimeoutMultiplier = 0;
    this->ct.ReadTotalTimeoutConstant = 0;
    this->ct.WriteTotalTimeoutMultiplier = 0;
    this->ct.WriteTotalTimeoutConstant = 0;

    if ( 0 == ::SetCommTimeouts(this->hd, &this->ct) )
        return false;
    this->m_charIntervalTimeout = 10; //TODO: 10 msecs is default (Reinsurance)
    return true;
}

/* Prepares other parameters of the structures port configuration.
   Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
void NativeSerialEnginePrivate::prepareOtherOptions()
{
    this->cc.dcb.fBinary = true;
    this->cc.dcb.fInX = this->cc.dcb.fOutX = this->cc.dcb.fAbortOnError = this->cc.dcb.fNull = false;
}

/* Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::saveOldSettings()
{
    /* prepare COMMCONFIG */
    DWORD confSize = sizeof(COMMCONFIG);
    this->cc.dwSize = this->oldcc.dwSize = confSize;
    //Saved current COMMCONFIG device
    if ( 0 == ::GetCommConfig(this->hd, &this->oldcc, &confSize) )
        return false;
    //Get configure
    ::memcpy((void *)(&this->cc), (const void *)(&this->oldcc), confSize);

    /* prepare COMMTIMEOUTS */
    confSize = sizeof(COMMTIMEOUTS);
    //Saved current COMMTIMEOUTS device
    if ( 0 == ::GetCommTimeouts(this->hd, &this->oldct) )
        return false;
    //Get configure
    ::memcpy((void *)(&this->ct), (const void *)(&this->oldct), confSize);

    //Set flag "altered parameters is saved"
    this->m_oldSettingsIsSaved = true;
    return true;
}

/* Used only in method: NativeSerialEnginePrivate::nativeClose()
*/
bool NativeSerialEnginePrivate::restoreOldSettings()
{
    bool restoreResult = true;
    if (this->m_oldSettingsIsSaved) {
        if ( 0 == ::SetCommConfig(this->hd, &this->oldcc, sizeof(COMMCONFIG)) )
            restoreResult = false;
        if ( 0 == ::SetCommTimeouts(this->hd, &this->oldct) )
            restoreResult = false;
    }

    return restoreResult;
}

/* Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::createEvents(bool evRx, bool evTx)
{
    if (evRx) { this->hEvRx = ::CreateEvent(0, false, false, 0); }
    if (evTx) { this->hEvTx = ::CreateEvent(0, false, false, 0); }
    this->hEvSelect = ::CreateEvent(0, false, false, 0);

    return ( (evRx && (0 == this->hEvRx))
             || (evTx && (0 == this->hEvTx))
             || (0 == this->hEvSelect) ) ?
            false : true;
}

/* Used only in method: NativeSerialEnginePrivate::nativeClose()
*/
bool NativeSerialEnginePrivate::closeEvents() const
{
    bool closeResult = true;
    if ( (this->hEvRx) && (0 == ::CloseHandle(this->hEvRx)) )
        closeResult = false;
    if ( (this->hEvTx) && (0 == ::CloseHandle(this->hEvTx) ) )
        closeResult = false;
    if ( (this->hEvSelect) && (0 == ::CloseHandle(this->hEvSelect)) )
        closeResult = false;

    return closeResult;
}


//------------------------------------------------------------------------------------------//

bool NativeSerialEngine::isReadNotificationEnabled() const
{
    Q_D(const NativeSerialEngine);
    return d->readNotifier && d->readNotifier->isEnabled();
}

void NativeSerialEngine::setReadNotificationEnabled(bool enable)
{
    Q_D(NativeSerialEngine);
    if (d->readNotifier) {
        d->readNotifier->setEnabled(enable);
    }
    else {
        if (enable && QAbstractEventDispatcher::instance(thread())) {
            d->readNotifier = new WinSerialNotifier(d->hd, WinSerialNotifier::Read, this);
            QObject::connect(d->readNotifier, SIGNAL(activated(HANDLE)), this, SIGNAL(readNotification()));
            d->readNotifier->setEnabled(true);
        }
    }
}
