/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundathis->tion; either version 2 of the License, or
* (at your opthis->tion) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundathis->tion, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/

#include <QtCore/QDir>
#include <QtCore/QAbstractEventDispatcher>

#include "../qcore_unix_p.h"

#include <errno.h>
#include <sys/time.h>
#include <sys/ioctl.h>

#include "nativeserialengine.h"


//#define NATIVESERIALENGINE_UNIX_DEBUG

#ifdef NATIVESERIALENGINE_UNIX_DEBUG
#include <QtCore/QDebug>
#endif


NativeSerialEnginePrivate::NativeSerialEnginePrivate()
    : fd(-1), readNotifier(0)
{
    ::memset((void *)(&this->tio), 0, sizeof(this->tio));
    ::memset((void *)(&oldtio), 0, sizeof(oldtio));

    this->m_deviceName = this->m_defaultDeviceName;
    //
    this->m_openMode = AbstractSerial::NotOpen;
    this->m_status = AbstractSerial::ENone;
    this->m_oldSettingsIsSaved = false;
}

bool NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
{
    if (this->isValid())  {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> The device is already open or other error. Error! \n";
#endif
        return false;
    }

    //here chek locked device or not
    this->locker.setDeviceName(this->m_deviceName);
    bool byCurrPid = false;
    if ( this->locker.locked(&byCurrPid) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> can not open the device:" << this->m_deviceName <<
                "\n, because it was locked. Error! \n";
#endif
        return false;
    }

    int flags = ( O_NOCTTY | O_NDELAY );
    switch (mode) {
    case AbstractSerial::ReadOnly:
        flags |= ( O_RDONLY ); break;
    case AbstractSerial::WriteOnly:
        flags |= ( O_WRONLY ); break;
    case AbstractSerial::ReadWrite:
        flags |= ( O_RDWR ); break;
    default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> mode: " << mode << " undefined. Error! \n";
#endif
        return false;
    }//switch

    //try opened serial device
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
    qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
            " -> trying to open device: " << this->m_deviceName << " \n";
#endif

    this->fd = qt_safe_open(this->m_deviceName.toLocal8Bit().constData(), flags);

    if (!this->isValid()) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: ::open(...) returned: -1,"
                "errno:" << errno << ". Error! \n";
#endif
        return false;
    }

    //here try lock device
    this->locker.lock();
    if (!this->locker.locked(&byCurrPid)) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> an error occurred when locking the device:" << this->m_deviceName << ". Error! \n";
#endif
        return false;
    }

/*
    if (!this->nativeReset()) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: nativeReset() returned: false. Error! \n";
#endif
        return false;
    }
*/

    if (!this->saveOldSettings()) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: saveOldSettings() returned: false. Error! \n";
#endif
        return false;
    }

    //Prepare other options
    this->prepareOtherOptions();

    if (!this->setDefaultAsyncCharTimeout()) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: setDefaultAsyncCharTimeout() returned: false. Error! \n";
#endif
        return false;
    }

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }

    if (!this->detectDefaultCurrentSettings()) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
                " -> function: detectDefaultCurrentSettings() returned: false. Error! \n";
#endif
        return false;
    }

#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
    qDebug() << "Linux: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode) \n"
            " -> opened device: " << this->m_deviceName << " in mode: " << mode << " succesfully. Ok! \n";
#endif

    this->m_openMode = mode;
    return true;
}

bool NativeSerialEnginePrivate::nativeClose()
{
    if (!this->isValid()) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeClose() \n"
               " -> descriptor is invalid. Error! \n");
#endif
        return false;
    }

    bool closeResult = true;

    //try restore old parameters device
    if (!this->restoreOldSettings()) {
#if defined (NATIVESERIALENGINE_WIN_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeClose() \n"
               " -> function: restoreOldSettings() returned: false. Error! \n");
#endif
        closeResult = false;
    }

    //try closed device
    if ( -1 == qt_safe_close(this->fd) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeClose() \n"
               " -> function: ::close(this->fd) returned: -1. Error! \n");
#endif
        closeResult = false;
    }

    //here try unlock device
    this->locker.setDeviceName(this->m_deviceName);
    bool byCurrPid = false;

    if ( this->locker.locked(&byCurrPid) && byCurrPid )
        this->locker.unlock();

    if (closeResult) {
        this->m_openMode = AbstractSerial::NotOpen;
        this->fd = -1;
    }

    return closeResult;
}

bool NativeSerialEnginePrivate::nativeSetBaudRate(AbstractSerial::BaudRate baudRate)
{
    return ( this->nativeSetInputBaudRate(baudRate)
             && this->nativeSetOutputBaudRate(baudRate) );
}

bool NativeSerialEnginePrivate::nativeSetInputBaudRate(AbstractSerial::BaudRate baudRate)
{
    speed_t br = 0;
    switch (baudRate) {
    case AbstractSerial::BaudRate14400:
    case AbstractSerial::BaudRate56000:
    case AbstractSerial::BaudRate76800:
    case AbstractSerial::BaudRate128000:
    case AbstractSerial::BaudRate256000:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetInputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> POSIX does not support baudRate: " << baudRate << " baud operathis->tion. Error! \n";
#endif
        return false;
    case AbstractSerial::BaudRate50: br = B50; break;
    case AbstractSerial::BaudRate75: br = B75; break;
    case AbstractSerial::BaudRate110: br = B110; break;
    case AbstractSerial::BaudRate134: br = B134; break;
    case AbstractSerial::BaudRate150: br = B150; break;
    case AbstractSerial::BaudRate200: br = B200; break;
    case AbstractSerial::BaudRate300: br = B300; break;
    case AbstractSerial::BaudRate600: br = B600; break;
    case AbstractSerial::BaudRate1200: br = B1200; break;
    case AbstractSerial::BaudRate1800: br = B1800; break;
    case AbstractSerial::BaudRate2400: br = B2400; break;
    case AbstractSerial::BaudRate4800: br = B4800; break;
    case AbstractSerial::BaudRate9600: br = B9600; break;
    case AbstractSerial::BaudRate19200: br = B19200; break;
    case AbstractSerial::BaudRate38400: br = B38400; break;
    case AbstractSerial::BaudRate57600: br = B57600; break;
    case AbstractSerial::BaudRate115200: br = B115200; break;
    default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetInputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> baudRate: " << baudRate << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::BaudRate. Error! \n";
#endif
        return false;
    }//switch baudrate

    if ( -1 == ::cfsetispeed(&this->tio, br) )  {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetInputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> function: ::cfsetispeed(...) returned: -1. Error! \n";
#endif
        return false;
    }

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetInputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }

    this->m_ibaudRate = baudRate;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetOutputBaudRate(AbstractSerial::BaudRate baudRate)
{
    speed_t br = 0;
    switch (baudRate) {
    case AbstractSerial::BaudRate14400:
    case AbstractSerial::BaudRate56000:
    case AbstractSerial::BaudRate76800:
    case AbstractSerial::BaudRate128000:
    case AbstractSerial::BaudRate256000:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetOutputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> POSIX does not support baudRate: " << baudRate << " baud operathis->tion. Error! \n";
#endif
        return false;
    case AbstractSerial::BaudRate50: br = B50; break;
    case AbstractSerial::BaudRate75: br = B75; break;
    case AbstractSerial::BaudRate110: br = B110; break;
    case AbstractSerial::BaudRate134: br = B134; break;
    case AbstractSerial::BaudRate150: br = B150; break;
    case AbstractSerial::BaudRate200: br = B200; break;
    case AbstractSerial::BaudRate300: br = B300; break;
    case AbstractSerial::BaudRate600: br = B600; break;
    case AbstractSerial::BaudRate1200: br = B1200; break;
    case AbstractSerial::BaudRate1800: br = B1800; break;
    case AbstractSerial::BaudRate2400: br = B2400; break;
    case AbstractSerial::BaudRate4800: br = B4800; break;
    case AbstractSerial::BaudRate9600: br = B9600; break;
    case AbstractSerial::BaudRate19200: br = B19200; break;
    case AbstractSerial::BaudRate38400: br = B38400; break;
    case AbstractSerial::BaudRate57600: br = B57600; break;
    case AbstractSerial::BaudRate115200: br = B115200; break;
    default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetOutputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> baudRate: " << baudRate << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::BaudRate. Error! \n";
#endif
        return false;
    }//switch baudrate

    if ( -1 == ::cfsetospeed(&this->tio, br) )  {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetOutputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> function: ::cfsetispeed(...) or function: ::cfsetospeed(...) returned: -1. Error! \n";
#endif
        return false;
    }

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetOutputBaudRate(AbstractSerial::BaudRate baudRate) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }
    this->m_obaudRate = baudRate;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits)
{
    if ( (AbstractSerial::DataBits5 == dataBits)
        && (AbstractSerial::StopBits2 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 5 data bits cannot be used with 2 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits6 == dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 6 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits7 == dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 7 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

    if ( (AbstractSerial::DataBits8 == dataBits)
        && (AbstractSerial::StopBits1_5 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
               " -> 8 data bits cannot be used with 1.5 stop bits. Error! \n");
#endif
        return false;
    }

#if !defined (CMSPAR)
    /* This protection against the installation of parity in the absence of macro Space CMSPAR.
    (That is prohibited for any *.nix than GNU/Linux) */

    if ( (AbstractSerial::ParitySpace == this->m_parity)
        && (AbstractSerial::DataBits8 == dataBits) ) {

#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                " -> parity: " << parity << " is not supported by the class NativeSerialEnginePrivate, \n"
                "space parity is only supported in POSIX with 7 or fewer data bits. Error! \n";
#endif
        return false;
    }
#endif

    switch (dataBits) {
    case AbstractSerial::DataBits5:
        this->tio.c_cflag &= (~CSIZE);
        this->tio.c_cflag |= CS5;
        break;
    case AbstractSerial::DataBits6:
        this->tio.c_cflag &= (~CSIZE);
        this->tio.c_cflag |= CS6;
        break;
    case AbstractSerial::DataBits7:
        this->tio.c_cflag &= (~CSIZE);
        this->tio.c_cflag |= CS7;
        break;
    case AbstractSerial::DataBits8:
        this->tio.c_cflag &= (~CSIZE);
        this->tio.c_cflag |= CS8;
        break;
    default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
                " -> dataBits: " << dataBits << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::DataBits. Error! \n";
#endif
        return false;
    }//switch dataBits

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetDataBits(AbstractSerial::DataBits dataBits) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }
    this->m_dataBits = dataBits;
    return true;
}

/*  TODO: in the future to correct method: nativeSetParity (), add  emulathis->tion mode parity Mark/Space and check CMSPAR.
*/
bool NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity)
{
    switch (parity) {
#if defined (CMSPAR)
    /* Here Installation parity only for GNU/Linux where the macro CMSPAR
    */
    case AbstractSerial::ParitySpace:
        this->tio.c_cflag &= ( ~PARODD );
        this->tio.c_cflag |= CMSPAR;
        break;
    case AbstractSerial::ParityMark:
        this->tio.c_cflag |= ( CMSPAR | PARODD );
        break;
#else //CMSPAR
    /* here setting parity to other *.nix.
        TODO: check here impl!
    */
    case AbstractSerial::ParitySpace:
        switch (this->m_dataBits) {
        case AbstractSerial::DataBits5:
            this->tio.c_cflag &= ~( PARENB | CSIZE );
            this->tio.c_cflag |= CS6;
            break;
        case AbstractSerial::DataBits6:
            this->tio.c_cflag &= ~( PARENB | CSIZE );
            this->tio.c_cflag |= CS7;
            break;
        case AbstractSerial::DataBits7:
            this->tio.c_cflag &= ~( PARENB | CSIZE );
            this->tio.c_cflag |= CS8;
            break;
        case AbstractSerial::DataBits8:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
            qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                    " -> parity: " << parity << " is not supported by the class NativeSerialEnginePrivate, \n"
                    "space parity is only supported in POSIX with 7 or fewer data bits. Error! \n";
#endif
            return false;
        default: ;
        }
        break;
        case AbstractSerial::ParityMark:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                " -> parity: " << parity << " is not supported by the class NativeSerialEnginePrivate, \n"
                "mark parity is not supported by POSIX.. Error! \n";
#endif
        return false;
#endif //CMSPAR

        case AbstractSerial::ParityNone:
        this->tio.c_cflag &= ( ~PARENB );
        break;
        case AbstractSerial::ParityEven:
        this->tio.c_cflag &= ( ~PARODD );
        this->tio.c_cflag |= PARENB;
        break;
        case AbstractSerial::ParityOdd:
        this->tio.c_cflag |= ( PARENB | PARODD );
        break;
        default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                " -> parity: " << parity << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::Parity. Error! \n";
#endif
        return false;
    }//switch parity

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetParity(AbstractSerial::Parity parity) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }
    this->m_parity = parity;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits)
{
    if ( (AbstractSerial::DataBits5 == this->m_dataBits)
        && (AbstractSerial::StopBits2 == this->m_stopBits) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
               " -> 5 data bits cannot be used with 2 stop bits. Error! \n");
#endif
        return false;
    }

    switch (stopBits) {
    case AbstractSerial::StopBits1:
        this->tio.c_cflag &= ( ~CSTOPB );
        break;
    case AbstractSerial::StopBits2:
        this->tio.c_cflag |= CSTOPB;
        break;
    case AbstractSerial::StopBits1_5:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
               " -> 1.5 stop bit operathis->tion is not supported by POSIX. Error! \n");
#endif
        return false;

    default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
                " -> stopBits: " << stopBits << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::StopBits. Error! \n";
#endif
        return false;
    }//switch

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetStopBits(AbstractSerial::StopBits stopBits) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }
    this->m_stopBits = stopBits;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetFlowControl(AbstractSerial::Flow flow)
{
    switch (flow) {
    case AbstractSerial::FlowControlOff:
        this->tio.c_cflag &= (~CRTSCTS);
        this->tio.c_iflag &= ( ~(IXON | IXOFF | IXANY ) );
        break;
    case AbstractSerial::FlowControlXonXoff:
        this->tio.c_cflag &= (~CRTSCTS);
        this->tio.c_iflag |= (IXON | IXOFF | IXANY);
        break;
    case AbstractSerial::FlowControlHardware:
        this->tio.c_cflag |= CRTSCTS;
        this->tio.c_iflag &= ( ~(IXON | IXOFF | IXANY) );
        break;
    default:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetFlowControl(AbstractSerial::Flow flow) \n"
                " -> flow: " << flow << " is not supported by the class NativeSerialEnginePrivate, \n"
                "see enum AbstractSerial::Flow. Error! \n";
#endif
        return false;
    }//switch

    if ( -1 == ::tcsetattr(this->fd, TCSANOW, &this->tio) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeSetFlowControl(AbstractSerial::Flow flow) \n"
                " -> function: ::tcsetattr(this->fd, TCSANOW, &this->tio) returned: -1. Error! \n";
#endif
        return false;
    }
    this->m_flow = flow;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetCharIntervalTimeout(int msecs)
{
    this->m_charIntervalTimeout = msecs;
    return true;
}

bool NativeSerialEnginePrivate::nativeSetDtr(bool set)
{
    int status = 0;

    if ( -1 == ::ioctl(this->fd, TIOCMGET, &status) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetDtr(bool set) \n"
               " -> function: ::ioctl(this->fd, TIOCMGET, &status) returned: -1. Error! \n");
#endif
        return false;
    }

    (set) ? status |= TIOCM_DTR : status &= (~TIOCM_DTR);

    if ( -1 == ::ioctl(this->fd, TIOCMSET, &status) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetDtr(bool set) \n"
               " -> function: ::ioctl(this->fd, TIOCMSET, &status) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

bool NativeSerialEnginePrivate::nativeSetRts(bool set)
{
    int status = 0;

    if ( -1 == ::ioctl(this->fd, TIOCMGET, &status) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetRts(bool set) \n"
               " -> function: ::ioctl(this->fd, TIOCMGET, &status) returned: -1. Error! \n");
#endif
        return false;
    }

    (set) ? status |= TIOCM_RTS : status &= (~TIOCM_RTS);

    if ( -1 == ::ioctl(this->fd, TIOCMSET, &status) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetRts(bool set) \n"
               " -> function: ::ioctl(this->fd, TIOCMSET, &status) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

ulong NativeSerialEnginePrivate::nativeLineStatus()
{
    ulong temp = 0;
    ulong status = 0;

    if ( -1 == ::ioctl(this->fd, TIOCMGET, &temp) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeLineStatus() \n"
               " -> function: ::ioctl(this->fd, TIOCMGET, &temp) returned: -1. Error! \n");
#endif
        return status;
    }

    if (temp & TIOCM_CTS) status |= AbstractSerial::lsCTS;
    if (temp & TIOCM_DSR) status |= AbstractSerial::lsDSR;
    if (temp & TIOCM_RI) status |= AbstractSerial::lsRI;
    if (temp & TIOCM_CD) status |= AbstractSerial::lsDCD;
    if (temp & TIOCM_DTR) status |= AbstractSerial::lsDTR;
    if (temp & TIOCM_RTS) status |= AbstractSerial::lsRTS;
    if (temp & TIOCM_ST) status |= AbstractSerial::lsST;
    if (temp & TIOCM_SR) status |= AbstractSerial::lsSR;

    return status;
}

bool NativeSerialEnginePrivate::nativeSendBreak(int duration)
{
    if ( -1 == ::tcsendbreak(this->fd, duration) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetRts(bool set) \n"
               " -> function: ::tcsendbreak(this->fd, duration) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

bool NativeSerialEnginePrivate::nativeSetBreak(bool set)
{
    if (set) {
        if ( -1 == ::ioctl(this->fd, TIOCSBRK) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
            qDebug("Linux: NativeSerialEnginePrivate::nativeSetBreak(bool set) \n"
                   " -> function: ::ioctl(this->fd, TIOCSBRK) returned: -1. Error! \n");
#endif
            return false;
        }
        return true;
    }

    if ( -1 == ::ioctl(this->fd, TIOCCBRK) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeSetBreak(bool set) \n"
               " -> function: ::ioctl(this->fd, TIOCCBRK) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

bool NativeSerialEnginePrivate::nativeFlush()
{
    if ( -1 == ::tcdrain(this->fd) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeFlush() \n"
               " -> function: ::tcdrain(this->fd) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

bool NativeSerialEnginePrivate::nativeReset()
{
    if ( -1 == ::tcflush(this->fd, TCIOFLUSH) ) {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug("Linux: NativeSerialEnginePrivate::nativeReset() \n"
               " -> function: ::tcflush(this->fd, TCIOFLUSH) returned: -1. Error! \n");
#endif
        return false;
    }
    return true;
}

qint64 NativeSerialEnginePrivate::nativeBytesAvailable()
{
    size_t nbytes = 0;
    //TODO: whether there has been done to build a multi-platform *.nix. (FIONREAD) ?
#if defined (FIONREAD)
    if ( -1 == ::ioctl(this->fd, FIONREAD, &nbytes) )
#else
    if ( -1 == ::ioctl(this->fd, TIOCINQ, &nbytes) )
#endif
    {
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
            qDebug("Linux: NativeSerialEnginePrivate::nativeBytesAvailable(bool wait) \n"
                   " -> function: ::ioctl(this->fd, FIONREAD, &nbytes) returned: -1. Error! \n");
#endif
        nbytes = -1;
    }
    return qint64(nbytes);
}

qint64 NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len)
{
    ssize_t bytesWritten = 0;
    do {
        bytesWritten = qt_safe_write(this->fd, (const void *)data, (size_t)len);
    } while ( (bytesWritten < 0) && (EINTR == errno) );

    this->nativeFlush();

    if (bytesWritten < 0) {
        switch (errno) {
        case EPIPE:
        case ECONNRESET:
            bytesWritten = -1;
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
            qDebug() << "Linux: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) write fail: \n"
                    " - bytes written, bytesWritten: " << bytesWritten << " bytes \n"
                    " - error code,           errno: " << errno << " \n. Error! \n";
#endif
            this->nativeClose();
            break;
        case EAGAIN:
            bytesWritten = 0;
            break;
        case EMSGSIZE:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
            qDebug() << "Linux: NativeSerialEnginePrivate::nativeWrite(const char *data, qint64 len) write fail: \n"
                    " - bytes written, bytesWritten: " << bytesWritten << " bytes \n"
                    " - error code,           errno: " << errno << " \n. Error! \n";
#endif
            break;
        default:;
        }
    }

    return quint64(bytesWritten);
}

qint64 NativeSerialEnginePrivate::nativeRead(char *data, qint64 len)
{
    ssize_t bytesReaded = 0;
    do {
        bytesReaded = qt_safe_read(this->fd, (void*)data, len);
    } while ( (-1 == bytesReaded) && (EINTR == errno) );

    if (bytesReaded < 0) {
        bytesReaded = -1;
        switch (errno) {
#if EWOULDBLOCK-0 && EWOULDBLOCK != EAGAIN
        case EWOULDBLOCK:
#endif
        case EAGAIN:
            // No data was available for reading
            bytesReaded = -2;
            break;
        case EBADF:
        case EINVAL:
        case EIO:
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::nativeRead(char *data, qint64 len) \n -> read fail: \n"
                " - bytes readed, bytesReaded: " << bytesReaded << " bytes \n"
                " - error code,         errno: " << errno << " \n. Error! \n";
#endif
            break;
#ifdef Q_OS_SYMBIAN
        case EPIPE:
#endif
        case ECONNRESET:
#if defined(Q_OS_VXWORKS)
        case ESHUTDOWN:
#endif
            bytesReaded = 0;
            break;
        default:;
        }
    }

    return quint64(bytesReaded);
}

int NativeSerialEnginePrivate::nativeSelect(int timeout,
                                            bool checkRead, bool checkWrite,
                                            bool *selectForRead, bool *selectForWrite)
{
    fd_set fdread;
    FD_ZERO(&fdread);
    if (checkRead)
        FD_SET(this->fd, &fdread);

    fd_set fdwrite;
    FD_ZERO(&fdwrite);
    if (checkWrite)
        FD_SET(this->fd, &fdwrite);

    struct timeval tv;
    tv.tv_sec = (timeout / 1000);
    tv.tv_usec = (timeout % 1000) * 1000;

    int ret = qt_safe_select(this->fd + 1, &fdread, &fdwrite, 0, (timeout < 0) ? 0 : &tv);
    //int ret = ::select(this->fd + 1, &fdread, 0, 0, timeout < 0 ? 0 : &tv);

    if (ret <= 0) {
        *selectForRead = *selectForWrite = false;
        return ret;
    }

    *selectForRead = FD_ISSET(this->fd, &fdread);
    *selectForWrite = FD_ISSET(this->fd, &fdwrite);

    return ret;
}

//added 06.11.2009 (while is not used)
qint64 NativeSerialEnginePrivate::nativeCurrentQueue(NativeSerialEngine::ioQueue Queue) const
{
    Q_UNUSED(Queue)
    /* not supported */
    return qint64(-1);
}

bool NativeSerialEnginePrivate::isValid() const
{
    return ( -1 != this->fd );
}

/* Defines a parameter - the current baud rate of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultBaudRate()
{
    //TODO: ??
    for (int i = 0; i < 2; ++i) {
        AbstractSerial::BaudRate *pBR = 0;
        speed_t result = 0;

        switch (i) {
        case 0:
            pBR = &this->m_ibaudRate;
            result = ::cfgetispeed(&this->tio);
            break;
        case 1:
            pBR = &this->m_obaudRate;
            result = ::cfgetospeed(&this->tio);
            break;
        default: return false;
        }

        switch (result) {
        case B50: *pBR = AbstractSerial::BaudRate50; break;
        case B75: *pBR = AbstractSerial::BaudRate75; break;
        case B110: *pBR = AbstractSerial::BaudRate110; break;
        case B134: *pBR = AbstractSerial::BaudRate134; break;
        case B150: *pBR = AbstractSerial::BaudRate150; break;
        case B200: *pBR = AbstractSerial::BaudRate200; break;
        case B300: *pBR = AbstractSerial::BaudRate300; break;
        case B600: *pBR = AbstractSerial::BaudRate600; break;
        case B1200: *pBR = AbstractSerial::BaudRate1200; break;
        case B1800: *pBR = AbstractSerial::BaudRate1800; break;
        case B2400: *pBR = AbstractSerial::BaudRate2400; break;
        case B4800: *pBR = AbstractSerial::BaudRate4800; break;
        case B9600: *pBR = AbstractSerial::BaudRate9600; break;
        case B19200: *pBR = AbstractSerial::BaudRate19200; break;
        case B38400: *pBR = AbstractSerial::BaudRate38400; break;
        case B57600: *pBR = AbstractSerial::BaudRate57600; break;
        case B115200: *pBR = AbstractSerial::BaudRate115200;  break;
        default:
            *pBR = AbstractSerial::BaudRateUndefined;
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
            qDebug() << "Linux: NativeSerialEnginePrivate::detectDefaultBaudRate() \n"
                    " -> function: ::cfgetispeed(&this->tio) or ::cfgetispeed(&this->tio) returned undefined baud rate: " << result << " \n";
#endif
            ;
        }
        pBR = 0;
    }
    return true;
}

/* Defines a parameter - the current data bits of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultDataBits()
{
    tcflag_t result = this->tio.c_cflag;

    switch (result & CSIZE) {
    case CS5:
        this->m_dataBits = AbstractSerial::DataBits5; break;
    case CS6:
        this->m_dataBits = AbstractSerial::DataBits6; break;
    case CS7:
        this->m_dataBits = AbstractSerial::DataBits7; break;
    case CS8:
        this->m_dataBits = AbstractSerial::DataBits8; break;
    default:
        this->m_dataBits = AbstractSerial::DataBitsUndefined;
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
        qDebug() << "Linux: NativeSerialEnginePrivate::detectDefaultDataBits() \n"
                " -> data bits undefined, c_cflag is: " << result << " \n";
#endif
        ;
    }
    return true;
}

/* Defines a parameter - the current parity of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultParity()
{
    tcflag_t result = this->tio.c_cflag;

#if defined (CMSPAR)
    /* TODO: while impl only if defined CMSPAR !!!
    */
    if ( (result & CMSPAR)
        && (!(result & PARODD)) ) {
        this->m_parity = AbstractSerial::ParitySpace;
        return true;
    }

    if ( (result & CMSPAR)
        && (result & PARODD) ) {
        this->m_parity = AbstractSerial::ParityMark;
        return true;
    }
#endif

    if ( !(result & PARENB) ) {
        this->m_parity = AbstractSerial::ParityNone;
        return true;
    }

    if ( (result & PARENB)
        && (!(result & PARODD)) ) {
        this->m_parity = AbstractSerial::ParityEven;
        return true;
    }

    if ( (result & PARENB)
        && (result & PARODD) ) {
        this->m_parity = AbstractSerial::ParityOdd;
        return true;
    }

    this->m_parity = AbstractSerial::ParityUndefined;
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
    qDebug() << "Linux: NativeSerialEnginePrivate::detectDefaultParity() \n"
            " -> parity undefined, c_cflag is: " << result << "\n";
#endif
    return true;
}

/* Defines a parameter - the current stop bits of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultStopBits()
{
    tcflag_t result = this->tio.c_cflag;

    if ( !(result & CSTOPB) ) {
        this->m_stopBits = AbstractSerial::StopBits1;
        return true;
    }

    if (result & CSTOPB) {
        this->m_stopBits = AbstractSerial::StopBits2;
        return true;
    }

    this->m_stopBits = AbstractSerial::StopBitsUndefined;
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
    qDebug() << "Linux: NativeSerialEnginePrivate::detectDefaultStopBits() \n"
            " -> stop bits undefined, c_cflag is: " << result << " \n";
#endif
    return true;
}

/* Defines a parameter - the current flow control of default,
   which can be obtained only after the open of the device (port).
   Used only in method: NativeSerialEnginePrivate::detectDefaultCurrentSettings()
*/
bool NativeSerialEnginePrivate::detectDefaultFlowControl()
{
    if ( (!(this->tio.c_cflag & CRTSCTS))
        && (!(this->tio.c_iflag & (IXON | IXOFF | IXANY))) ) {
        this->m_flow = AbstractSerial::FlowControlOff;
        return true;
    }

    if ( (!(this->tio.c_cflag & CRTSCTS))
        && (this->tio.c_iflag & (IXON | IXOFF | IXANY)) ) {
        this->m_flow = AbstractSerial::FlowControlXonXoff;
        return true;
    }

    if ( (this->tio.c_cflag & CRTSCTS)
        && (!(this->tio.c_iflag & (IXON | IXOFF | IXANY))) ) {
        this->m_flow = AbstractSerial::FlowControlHardware;
        return true;
    }

    this->m_flow = AbstractSerial::FlowControlUndefined;
#if defined (NATIVESERIALENGINE_UNIX_DEBUG)
    qDebug() << "Linux: NativeSerialEnginePrivate::detectDefaultFlowControl() \n"
            " -> flow control undefined \n";
#endif
    return true;
}

/* Specifies the port settings that were with him by default when you open it.
   Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::detectDefaultCurrentSettings()
{
    return ( this->detectDefaultBaudRate() && this->detectDefaultDataBits() && this->detectDefaultParity()
             && this->detectDefaultStopBits() && this->detectDefaultFlowControl() );
}

/* Force sets the parameters of timeouts port for asynchronous mode.
   Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::setDefaultAsyncCharTimeout()
{
    /* TODO:
    here you need to specify the settings for the asynchronous mode! to check! */
    this->tio.c_cc[VTIME] = 0;
    this->tio.c_cc[VMIN] = 0;
    this->m_charIntervalTimeout = 10; //TODO: 10 msecs is default (Reinsurance)
    return true;
}

/* Prepares other parameters of the structures port configurathis->tion.
   Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
void NativeSerialEnginePrivate::prepareOtherOptions()
{
    //this->tio.c_line = 0;

    //control modes
    /*
       CBR  (not in POSIX) Baud speed mask (4+1 bits).  [requires _BSD_SOURCE or _SVID_SOURCE]
       CBREX
              (not in POSIX) Extra baud speed mask (1 bit), included in CBR.  [requires _BSD_SOURCE or _SVID_SOURCE]
              (POSIX says that the baud speed is stored in the termios structure without specifying where precisely, and  provides  cfgetispeed()
              and  cfsetispeed()  for  getting at it.  Some systems use bits selected by CBR in c_cflag, other systems use separate fields, for
              example, sg_ispeed and sg_ospeed.)
       CSIZE  Character size mask.  Values are CS5, CS6, CS7, or CS8.
       CSTOPB Set two stop bits, rather than one.
       CREAD  Enable receiver.
       PARENB Enable parity generathis->tion on output and parity checking for input.
       PARODD If set, then parity for input and output is odd; otherwise even parity is used.
       HUPCL  Lower modem control lines after last process closes the device (hang up).
       CLOCAL Ignore modem control lines.
       LOBLK  (not in POSIX) Block output from a non-current shell layer.  For use by shl (shell layers).  (Not implemented on Linux.)
       CIBR (not in POSIX) Mask for input speeds.  The values for the CIBR bits are the same as the values for the CBR bits,  shifted  left
              IBSHIFT bits.  [requires _BSD_SOURCE or _SVID_SOURCE] (Not implemented on Linux.)
       CMSPAR (not in POSIX) Use "stick" (mark/space) parity (supported on certain serial devices): if PARODD is set, the parity bit is always 1;
              if PARODD is not set, then the parity bit is always 0).  [requires _BSD_SOURCE or _SVID_SOURCE]
       CRTSCTS
              (not in POSIX) Enable RTS/CTS (hardware) flow control.  [requires _BSD_SOURCE or _SVID_SOURCE]
    */
    this->tio.c_cflag |= ( CREAD|CLOCAL );
    this->tio.c_cflag |= ( ~HUPCL );

    //local modes
    /*
       ISIG   When any of the characters INTR, QUIT, SUSP, or DSUSP are received, generate the corresponding signal.
       ICANON Enable canonical mode (described below).
       XCASE  (not in POSIX; not supported under Linux) If ICANON is also set, terminal is uppercase only.   Input  is  converted  to  lowercase,
              except  for  characters preceded by \.  On output, uppercase characters are preceded by \ and lowercase characters are converted to
              uppercase.  [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
       ECHO   Echo input characters.
       ECHOE  If ICANON is also set, the ERASE character erases the preceding input character, and WERASE erases the preceding word.
       ECHOK  If ICANON is also set, the KILL character erases the current line.
       ECHONL If ICANON is also set, echo the NL character even if ECHO is not set.
       ECHOCTL
              (not in POSIX) If ECHO is also set, ASCII control signals other than TAB, NL, START, and STOP are echoed as  ^X,  where  X  is  the
              character  with  ASCII  code  0x40  greater  than the control signal.  For example, character 0x08 (BS) is echoed as ^H.  [requires
              _BSD_SOURCE or _SVID_SOURCE]
       ECHOPRT
              (not in POSIX) If ICANON and IECHO are also set, characters are printed  as  they  are  being  erased.   [requires  _BSD_SOURCE  or
              _SVID_SOURCE]
       ECHOKE (not  in  POSIX)  If  ICANON  is also set, KILL is echoed by erasing each character on the line, as specified by ECHOE and ECHOPRT.
              [requires _BSD_SOURCE or _SVID_SOURCE]
       DEFECHO
              (not in POSIX) Echo only when a process is reading.  (Not implemented on Linux.)
       FLUSHO (not in POSIX; not supported under Linux) Output is being  flushed.   This  flag  is  toggled  by  typing  the  DISCARD  character.
              [requires _BSD_SOURCE or _SVID_SOURCE]
       NOFLSH Disable flushing the input and output queues when generating the SIGINT, SIGQUIT, and SIGSUSP signals.
       TOSTOP Send the SIGTTOU signal to the process group of a background process which tries to write to its controlling terminal.
       PENDIN (not  in  POSIX;  not  supported  under  Linux)  All  characters  in the input queue are reprinted when the next character is read.
              (bash(1) handles typeahead this way.)  [requires _BSD_SOURCE or _SVID_SOURCE]
       IEXTEN Enable implementathis->tion-defined input processing.  This flag, as well as ICANON must be enabled for   the  special  characters  EOL2,
              LNEXT, REPRINT, WERASE to be interpreted, and for the IUCLC flag to be effective.
    */
    this->tio.c_lflag &= ( ~(ICANON | ECHO | ISIG) );
    this->tio.c_lflag |= (IEXTEN);

    //input modes
    /*
       IGNBRK Ignore BREAK condithis->tion on input.
       BRKINT If IGNBRK is set, a BREAK is ignored.  If it is not set but BRKINT is set, then a BREAK causes the input and output  queues  to  be
              flushed,  and  if the terminal is the controlling terminal of a foreground process group, it will cause a SIGINT to be sent to this
              foreground process group.  When neither IGNBRK nor BRKINT are set, a BREAK reads as a 0 byte ('\0'), except when PARMRK is  set,
              in which case it reads as the sequence \377 \0 \0.
       IGNPAR Ignore framing errors and parity errors.
       PARMRK If  IGNPAR  is not set, prefix a character with a parity error or framing error with \377 \0.  If neither IGNPAR nor PARMRK is set,
              read a character with a parity error or framing error as \0.
       INPCK  Enable input parity checking.
       ISTRIP Strip off eighth bit.
       INLCR  Translate NL to CR on input.
       IGNCR  Ignore carriage return on input.
       ICRNL  Translate carriage return to newline on input (unless IGNCR is set).
       IUCLC  (not in POSIX) Map uppercase characters to lowercase on input.
       IXON   Enable XON/XOFF flow control on output.
       IXANY  (XSI) Typing any character will restart stopped output.  (The default is to allow just the START character to restart output.)
       IXOFF  Enable XON/XOFF flow control on input.
    */
    this->tio.c_iflag &= ( ~(IGNPAR | PARMRK | ISTRIP | ICRNL | IGNCR | INLCR) );
    this->tio.c_iflag |= ( IGNBRK | INPCK | IXANY );

    //output modes
    /*
       OPOST  Enable implementathis->tion-defined output processing.
       The remaining c_oflag flag constants are defined in POSIX.1-2001, unless marked otherwise.
       OLCUC  (not in POSIX) Map lowercase characters to uppercase on output.
       ONLCR  (XSI) Map NL to CR-NL on output.
       OCRNL  Map CR to NL on output.
       ONOCR  Don't output CR at column 0.
       ONLRET Don't output CR.
       OFILL  Send fill characters for a delay, rather than using a timed delay.
       OFDEL  (not in POSIX) Fill character is ASCII DEL (0177).  If unset, fill character is ASCII NUL ('\0').  (Not implemented on Linux.)
       NLDLY  Newline delay mask.  Values are NL0 and NL1.  [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
       CRDLY  Carriage return delay mask.  Values are CR0, CR1, CR2, or CR3.  [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
       TABDLY Horizontal tab delay mask.  Values are TAB0, TAB1, TAB2, TAB3 (or XTABS).  A value of TAB3, that is, XTABS, expands tabs to  spaces
              (with tab stops every eight columns).  [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
       BSDLY  Backspace  delay  mask.   Values  are  BS0  or  BS1.   (Has  never  been  implemented.)   [requires  _BSD_SOURCE or _SVID_SOURCE or
              _XOPEN_SOURCE]
       VTDLY  Vertical tab delay mask.  Values are VT0 or VT1.
       FFDLY  Form feed delay mask.  Values are FF0 or FF1.  [requires _BSD_SOURCE or _SVID_SOURCE or _XOPEN_SOURCE]
    */
    this->tio.c_oflag &= ( ~(ONLCR | OCRNL | ONLRET | OFILL) );
    this->tio.c_oflag |= ( OPOST | ONOCR );

    //control chars
    /*
       VINTR  (003,  ETX,  Ctrl-C,  or also 0177, DEL, rubout) Interrupt character.  Send a SIGINT signal.  Recognized when ISIG is set, and then
              not passed as input.
       VQUIT  (034, FS, Ctrl-\) Quit character.  Send SIGQUIT signal.  Recognized when ISIG is set, and then not passed as input.
       VERASE (0177, DEL, rubout, or 010, BS, Ctrl-H, or also #) Erase character.  This erases the previous not-yet-erased  character,  but  does
              not erase past EOF or beginning-of-line.  Recognized when ICANON is set, and then not passed as input.
       VKILL  (025,  NAK,  Ctrl-U,  or Ctrl-X, or also @) Kill character.  This erases the input since the last EOF or beginning-of-line.  Recog‐
              nized when ICANON is set, and then not passed as input.
       VEOF   (004, EOT, Ctrl-D) End-of-file character.  More precisely: this character causes the pending tty buffer to be sent to  the  waiting
              user program without waiting for end-of-line.  If it is the first character of the line, the read(2) in the user program returns 0,
              which signifies end-of-file.  Recognized when ICANON is set, and then not passed as input.
       VMIN   Minimum number of characters for non-canonical read.
       VEOL   (0, NUL) Addithis->tional end-of-line character.  Recognized when ICANON is set.
       VTIME  Timeout in deciseconds for non-canonical read.
       VEOL2  (not in POSIX; 0, NUL) Yet another end-of-line character.  Recognized when ICANON is set.
       VSWTCH (not in POSIX; not supported under Linux; 0, NUL) Switch character.  (Used by shl only.)
       VSTART (021, DC1, Ctrl-Q) Start character.  Restarts output stopped by the Stop character.  Recognized when IXON  is  set,  and  then  not
              passed as input.
       VSTOP  (023,  DC3,  Ctrl-S) Stop character.  Stop output until Start character typed.  Recognized when IXON is set, and then not passed as
              input.
       VSUSP  (032, SUB, Ctrl-Z) Suspend character.  Send SIGTSTP signal.  Recognized when ISIG is set, and then not passed as input.
       VDSUSP (not in POSIX; not supported under Linux; 031, EM, Ctrl-Y) Delayed suspend character: send SIGTSTP signal  when  the  character  is
              read  by  the  user  program.  Recognized when IEXTEN and ISIG are set, and the system supports job control, and then not passed as
              input.
       VLNEXT (not in POSIX; 026, SYN, Ctrl-V) Literal next.  Quotes the next input character, depriving it of a possible special meaning.   Rec‐
              ognized when IEXTEN is set, and then not passed as input.
       VWERASE
              (not in POSIX; 027, ETB, Ctrl-W) Word erase.  Recognized when ICANON and IEXTEN are set, and then not passed as input.
       VREPRINT
              (not  in  POSIX;  022,  DC2,  Ctrl-R) Reprint unread characters.  Recognized when ICANON and IEXTEN are set, and then not passed as
              input.
       VDISCARD
              (not in POSIX; not supported under Linux; 017, SI, Ctrl-O) Toggle: start/stop discarding pending output.  Recognized when IEXTEN is
              set, and then not passed as input.
       VSTATUS
              (not in POSIX; not supported under Linux; status request: 024, DC4, Ctrl-T).
    */
    /*
        this->tio.c_cc[VINTR]=_POSIX_VDISABLE;
        this->tio.c_cc[VQUIT]=_POSIX_VDISABLE;
        this->tio.c_cc[VSTART]=_POSIX_VDISABLE;
        this->tio.c_cc[VSTOP]=_POSIX_VDISABLE;
        this->tio.c_cc[VSUSP]=_POSIX_VDISABLE;
    */
}

/* Used only in method: NativeSerialEnginePrivate::nativeOpen(AbstractSerial::OpenMode mode)
*/
bool NativeSerialEnginePrivate::saveOldSettings()
{
    //saved current settings to oldtio
    if ( -1 == ::tcgetattr(this->fd, &this->oldtio) )
        return false;
    //Get configure
    ::memcpy((void *)(&this->tio), (const void *)(&this->oldtio), sizeof(this->oldtio));

    //Set flag "altered parameters is saved"
    this->m_oldSettingsIsSaved = true;
    return true;
}

/* Used only in method: NativeSerialEnginePrivate::nativeClose()
*/
bool NativeSerialEnginePrivate::restoreOldSettings()
{
    return ( this->m_oldSettingsIsSaved
             && (-1 == ::tcsetattr(this->fd, TCSANOW, &this->oldtio)) ) ?
            false : true;
}


//------------------------------------------------------------------------------------------//

bool NativeSerialEngine::isReadNotificationEnabled() const
{
    Q_D(const NativeSerialEngine);
    return d->readNotifier && d->readNotifier->isEnabled();
}

void NativeSerialEngine::setReadNotificationEnabled(bool enable)
{
    Q_D(NativeSerialEngine);
    if (d->readNotifier) {
        d->readNotifier->setEnabled(enable);
    }
    else {
        if (enable && QAbstractEventDispatcher::instance(thread())) {
            d->readNotifier = new QSocketNotifier(d->fd, QSocketNotifier::Read, this);
            QObject::connect(d->readNotifier, SIGNAL(activated(int)), this, SIGNAL(readNotification()));
            d->readNotifier->setEnabled(true);
        }
    }
}
