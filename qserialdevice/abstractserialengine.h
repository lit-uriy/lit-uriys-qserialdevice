/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/

#ifndef ABSTRACTSERIALENGINE_H
#define ABSTRACTSERIALENGINE_H

#include "abstractserial.h"

class AbstractSerialEnginePrivate;
class AbstractSerialEngine : public QObject
{
    Q_OBJECT

Q_SIGNALS:
    void readNotification();

public:
    explicit AbstractSerialEngine(QObject *parent = 0);
    virtual ~AbstractSerialEngine();

    //TODO: Why static?
    static AbstractSerialEngine *createSerialEngine(QObject *parent);

    void setDeviceName(const QString &deviceName);
    QString deviceName() const;

    void setOpenMode(AbstractSerial::OpenMode mode);
    AbstractSerial::OpenMode openMode() const;

    virtual bool open(AbstractSerial::OpenMode mode) = 0;
    bool isOpen() const;

    virtual void close() = 0;

    virtual bool setBaudRate(AbstractSerial::BaudRate baudRate) = 0;
    virtual bool setInputBaudRate(AbstractSerial::BaudRate baudRate) = 0;
    virtual bool setOutputBaudRate(AbstractSerial::BaudRate baudRate) = 0;
    AbstractSerial::BaudRate baudRate() const;
    AbstractSerial::BaudRate inputBaudRate() const;
    AbstractSerial::BaudRate outputBaudRate() const;

    virtual bool setDataBits(AbstractSerial::DataBits dataBits) = 0;
    AbstractSerial::DataBits dataBits() const;

    virtual bool setParity(AbstractSerial::Parity parity) = 0;
    AbstractSerial::Parity parity() const;

    virtual bool setStopBits(AbstractSerial::StopBits stopBits) = 0;
    AbstractSerial::StopBits stopBits() const;

    virtual bool setFlowControl(AbstractSerial::Flow flow) = 0;
    AbstractSerial::Flow flow() const;

    virtual bool setCharIntervalTimeout(int msecs) = 0;
    int charIntervalTimeout() const;

    virtual bool setDtr(bool set) = 0;
    virtual bool setRts(bool set) = 0;
    virtual ulong lineStatus() = 0;

    virtual bool sendBreak(int duration) = 0;
    virtual bool setBreak(bool set) = 0;

    virtual bool flush() = 0;
    virtual bool reset() = 0;

    virtual qint64 bytesAvailable() = 0;

    virtual qint64 read(char *data, qint64 maxSize) = 0;
    virtual qint64 write(const char *data, qint64 maxSize) = 0;

    virtual bool waitForReadOrWrite(bool *readyToRead, bool *readyToWrite,
                bool checkRead, bool checkWrite,
                int msecs = 3000) = 0;

    AbstractSerial::Status status() const;

    virtual bool isReadNotificationEnabled() const = 0;
    virtual void setReadNotificationEnabled(bool enable) = 0;

protected:
    AbstractSerialEnginePrivate * const d_ptr;
    AbstractSerialEngine(AbstractSerialEnginePrivate &dd, QObject *parent);

    //add 05.11.2009 (while is not used, reserved)
    virtual qint64 currentTxQueue() const = 0;
    virtual qint64 currentRxQueue() const = 0;

private:
    Q_DECLARE_PRIVATE(AbstractSerialEngine)
    Q_DISABLE_COPY(AbstractSerialEngine)
};

class AbstractSerialEnginePrivate
{
public:
    //TODO: right?
    static const char m_defaultDeviceName[];

    QString m_deviceName;

    AbstractSerial::BaudRate m_ibaudRate;
    AbstractSerial::BaudRate m_obaudRate;
    AbstractSerial::DataBits m_dataBits;
    AbstractSerial::Parity m_parity;
    AbstractSerial::StopBits m_stopBits;
    AbstractSerial::Flow m_flow;

    int m_charIntervalTimeout;

    AbstractSerial::Status m_status;
    AbstractSerial::OpenMode m_openMode;

    bool m_oldSettingsIsSaved;
};

#endif // ABSTRACTSERIALENGINE_H
