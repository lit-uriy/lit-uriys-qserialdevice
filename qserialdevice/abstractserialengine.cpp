/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/


#include "nativeserialengine.h"


#ifdef Q_OS_WIN32
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "COM1";
#elif defined(Q_OS_IRIX)
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "/dev/ttyf1";
#elif defined(Q_OS_HPUX)
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "/dev/tty1p0";
#elif defined(Q_OS_SOLARIS)
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "/dev/ttya";
#elif defined(Q_OS_FREEBSD)
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "/dev/ttyd1";
#elif defined(Q_OS_LINUX)
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "/dev/ttyS0";
#else
    const char AbstractSerialEnginePrivate::m_defaultDeviceName[] = "/dev/ttyS0";
#endif

//---------------------------------------------------------------------------//

AbstractSerialEngine::AbstractSerialEngine(QObject *parent)
    : QObject(parent), d_ptr(new AbstractSerialEnginePrivate())
{
}

AbstractSerialEngine::AbstractSerialEngine(AbstractSerialEnginePrivate &dd, QObject *parent)
    : QObject(parent), d_ptr(&dd)
{
}

AbstractSerialEngine::~AbstractSerialEngine()
{
    delete d_ptr;
}

AbstractSerialEngine *AbstractSerialEngine::createSerialEngine(QObject *parent)
{
    return new NativeSerialEngine(parent);
}

void AbstractSerialEngine::setDeviceName(const QString &deviceName)
{
    d_func()->m_deviceName = deviceName;
}

QString AbstractSerialEngine::deviceName() const
{
    return d_func()->m_deviceName;
}

void AbstractSerialEngine::setOpenMode(AbstractSerial::OpenMode mode)
{
    d_func()->m_openMode = mode;
}

AbstractSerial::OpenMode AbstractSerialEngine::openMode() const
{
    return d_func()->m_openMode;
}

bool AbstractSerialEngine::isOpen() const
{
    return (d_func()->m_openMode != AbstractSerial::NotOpen);
}

AbstractSerial::BaudRate AbstractSerialEngine::baudRate() const
{
    return (d_func()->m_ibaudRate == d_func()->m_obaudRate) ?
        d_func()->m_ibaudRate : AbstractSerial::BaudRateUndefined;
}

AbstractSerial::BaudRate AbstractSerialEngine::inputBaudRate() const
{
    return d_func()->m_ibaudRate;
}

AbstractSerial::BaudRate AbstractSerialEngine::outputBaudRate() const
{
    return d_func()->m_obaudRate;
}

AbstractSerial::DataBits AbstractSerialEngine::dataBits() const
{
    return d_func()->m_dataBits;
}

AbstractSerial::Parity AbstractSerialEngine::parity() const
{
    return d_func()->m_parity;
}

AbstractSerial::StopBits AbstractSerialEngine::stopBits() const
{
    return d_func()->m_stopBits;
}

AbstractSerial::Flow AbstractSerialEngine::flow() const
{
    return d_func()->m_flow;
}

int AbstractSerialEngine::charIntervalTimeout() const
{
    return d_func()->m_charIntervalTimeout;
}

AbstractSerial::Status AbstractSerialEngine::status() const
{
    return d_func()->m_status;
}
