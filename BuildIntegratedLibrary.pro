#
#PROJECT        = Integrated Serial Device Liblary
TEMPLATE        = lib

CONFIG          += staticlib
#CONFIG          += dll
CONFIG          -= debug_and_release debug
QT              -= gui

OBJECTS_DIR     = build/lib/qintegratedserialdevice/obj
MOC_DIR         = build/lib/qintegratedserialdevice/moc

include(qserialdevice/qserialdevice.pri)
include(qserialdeviceinfo/qserialdeviceinfo.pri)
include(qserialdevicewatcher/qserialdevicewatcher.pri)

unix {
    LIBS        += -ludev
}

#TODO: here in future replace
#contains( CONFIG, staticlib ) {
#   win32:DEFINES += QSERIALDEVICE_STATIC
#}

#TODO: here in future replace
contains( CONFIG, dll ) {
    win32:DEFINES += QSERIALDEVICE_SHARED
    win32:RC_FILE   += version.rc
}

DESTDIR         = build/lib/qintegratedserialdevice/release
TARGET          = qserialdevice

VERSION         = 0.2.0

#message("CONFIG = $$CONFIG")
#message("DEFINES = $$DEFINES")
